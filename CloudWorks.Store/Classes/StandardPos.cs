﻿using CloudWorks.Store.BaseClasses;
using System;

namespace CloudWorks.Store.Classes
{
    public sealed class StandardPos : Pos
    {
        private Employee _employee;

        public StandardPos(int posNumber, Employee employee) : base(posNumber)
        {
            _employee = employee;
        }

        public override double Checkout(Client client) =>
            _employee.CheckoutSpeed * client.Items.Length;

        public override void Print() => Console.WriteLine($"StandardPos: {PosNumber}, Employee: {_employee.Name}.");
    }
}
