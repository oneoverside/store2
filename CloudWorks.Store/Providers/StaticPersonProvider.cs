﻿using CloudWorks.Store.BaseClasses;
using CloudWorks.Store.Classes;
using CloudWorks.Store.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudWorks.Store.Providers
{
    /// <inheritdoc/>
    public class StaticPersonProvider : IPersonProvider
    {
        /// <inheritdoc/>
        public Person[] GetPersons()
        {
            return new Person[] {
                new Client("Sergey", 2) {
                    Items = new Product []
                    {
                        new Product()
                        {
                             Name = "Ball"
                        },
                        new Product()
                        {
                             Name = "Backet"
                        },
                    }
                },
                new Client("Aleksander", 1) {
                    Items = new Product []
                    {
                        new Product()
                        {
                             Name = "Toy"
                        },
                        new Product()
                        {
                             Name = "Book"
                        },
                    }
                },
                new Employee("Nikolay") {
                },
                new Employee("Nina", 3) {
                },
            };

        }
    }
}
