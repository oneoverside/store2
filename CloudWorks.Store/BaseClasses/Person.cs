﻿using CloudWorks.Store.Attributes;
using CloudWorks.Store.Interfaces;

namespace CloudWorks.Store.BaseClasses
{
    public abstract class Person : IPrintable
    {
        protected Person(string name, double checkoutSpeed)
        {
            Name = name;
            CheckoutSpeed = checkoutSpeed;
        }
        
        [StringLength(10)]
        public string Name { get; private set; }

        public double CheckoutSpeed { get; private set; }
        public abstract void Print();
    }
}
