﻿using System;
using System.Diagnostics;

namespace CloudWorks.Store.Utils
{

    /// <summary>
    /// Helper for measuring time.
    /// </summary>
    public class TimingMeasuringHelper
    {
        /// <summary>
        /// Function that help to measuring time for action.
        /// </summary>
        /// <param name="action">The action that should be measuring.</param>
        /// <param name="iterations">Count of iterations.</param>
        /// <returns></returns>
        public static TimeSpan Measuring(Action action, int iterations)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            
            for (int i = 0; i< iterations;i++)
            {
                action();
            }

            sw.Stop();
            return sw.Elapsed;
        }

    }


}
